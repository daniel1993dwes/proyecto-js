-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-10-2018 a las 10:12:28
-- Versión del servidor: 10.1.26-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `project1`
--
DROP DATABASE IF EXISTS `project1`;
CREATE DATABASE IF NOT EXISTS `project1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `project1`;

DELIMITER $$
--
-- Funciones
--
CREATE DEFINER=`root`@`localhost` FUNCTION `haversine` (`lat1` FLOAT, `lon1` FLOAT, `lat2` FLOAT, `lon2` FLOAT) RETURNS FLOAT NO SQL
    DETERMINISTIC
    COMMENT 'Returns the distance in degrees on the Earth\r\n             between two known points of latitude and longitude'
BEGIN
    RETURN DEGREES(
        	ACOS(
              COS(RADIANS(lat1)) *
              COS(RADIANS(lat2)) *
              COS(RADIANS(lon2) - RADIANS(lon1)) +
              SIN(RADIANS(lat1)) * SIN(RADIANS(lat2))
            )
    	  )*111.045;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `stars` smallint(6) NOT NULL,
  `restaurantId` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `text` varchar(1000) NOT NULL,
  `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comment`
--

INSERT INTO `comment` (`id`, `stars`, `restaurantId`, `userId`, `text`, `date`) VALUES
(17, 4, 15, 22, 'Good restaurant', '2018-10-19 13:27:56.614413'),
(20, 4, 18, 23, 'Hello', '2018-10-19 18:45:47.432331');

--
-- Disparadores `comment`
--
DELIMITER $$
CREATE TRIGGER `UPDATE_REST_STARS` AFTER INSERT ON `comment` FOR EACH ROW UPDATE restaurant SET stars = (
    SELECT AVG(stars) FROM `comment` 
    WHERE `comment`.restaurantId = NEW.restaurantId
) WHERE id = NEW.restaurantId
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `daysOpen` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `cuisine` text NOT NULL,
  `creatorId` int(10) UNSIGNED NOT NULL,
  `stars` decimal(4,2) NOT NULL DEFAULT '0.00',
  `lat` decimal(10,7) NOT NULL DEFAULT '0.0000000',
  `lng` decimal(10,7) NOT NULL DEFAULT '0.0000000',
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `restaurant`
--

INSERT INTO `restaurant` (`id`, `name`, `description`, `daysOpen`, `phone`, `image`, `cuisine`, `creatorId`, `stars`, `lat`, `lng`, `address`) VALUES
(15, 'New Restaurant', 'Description', '1,3,4,6,0', '345654765', 'img/restaurants/1539861221476.jpg', 'Mediterranean', 21, '4.00', '38.3412926', '-0.4913679', 'Calle Pintor Lorenzo Casanova, Alicante'),
(17, 'Another restaurant', 'Description', '1,3,5,6', '934234512', 'img/restaurants/1539948440806.jpg', 'Mediterranean', 22, '0.00', '37.2345235', '-1.4235000', 'Another street'),
(18, 'Restaurant Example', 'Description', '1,2,3,4,5,6', '645734523', 'img/restaurants/1539967381106.jpg', 'Nothing good', 23, '4.00', '40.4250922', '-3.6096826', 'Calle Butrón, Madrid');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL,
  `password` varchar(150) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL DEFAULT 'img/profile.jpg',
  `email` varchar(150) NOT NULL,
  `lat` decimal(10,7) NOT NULL DEFAULT '0.0000000',
  `lng` decimal(10,7) NOT NULL DEFAULT '0.0000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `avatar`, `email`, `lat`, `lng`) VALUES
(21, 'Arturo', 'A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=', 'img/users/1539860985840.jpg', 'arturo@iessanvicente.com', '38.3471226', '-0.4974451'),
(22, 'John Wayne', 'A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=', 'img/users/1539948671405.jpg', 'email2@email.com', '41.3254320', '-1.2345500'),
(23, 'Arturo Bernal', 'A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=', 'img/users/1539967775772.jpg', 'a@a.es', '38.4311794', '-0.5352448');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_80ac01df3ea33eb2e732f6db16` (`restaurantId`,`userId`),
  ADD KEY `FK_c0354a9a009d3bb45a08655ce3b` (`userId`);

--
-- Indices de la tabla `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_89043c1ad06e2e7ed9dab170e99` (`creatorId`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_15f084462e057a3958531cf88fc` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant` (`id`),
  ADD CONSTRAINT `FK_c0354a9a009d3bb45a08655ce3b` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `restaurant`
--
ALTER TABLE `restaurant`
  ADD CONSTRAINT `FK_89043c1ad06e2e7ed9dab170e99` FOREIGN KEY (`creatorId`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
