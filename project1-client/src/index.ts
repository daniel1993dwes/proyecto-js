'use strict';
import { Restaurant } from "./classes/restaurant.class";
import { Auth } from "./classes/auth.class";
import swal from "sweetalert2";


let container: HTMLDivElement = null;
let restaurants: Restaurant[] = [];
let orderName: boolean = false;
let showOpen:boolean = false;
let search: string = '';

document.addEventListener('DOMContentLoaded', async event => {
    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign('../login.html');
    }
    document.getElementById('logout').addEventListener('click', event => {
        Auth.logout();
    })
    container = <HTMLDivElement> document.getElementById('placesContainer');
    
    let b1 = <HTMLLinkElement> document.getElementById('orderName');
    let b2 = <HTMLLinkElement> document.getElementById('showOpen');
    let des = <HTMLInputElement> document.getElementById('search');

    try{
        restaurants = await Restaurant.getAll();
            ShowRestaurants(restaurants);
    } catch (e) {}

    b1.addEventListener('click', event =>{
        if(orderName == true){
            orderName = false;
            DesactivarB1(event);
        }else{
            orderName = true;
            ActivarB1(event);
        }
        ShowRestaurants(restaurants);
    });

    b2.addEventListener('click', event =>{
        if(showOpen == true){
            showOpen = false;
            DesactivarB2(event);
        }else{
            showOpen = true;
            ActivarB2(event);
        }
        ShowRestaurants(restaurants);
    });

    des.addEventListener('input', event =>{
        search = des.value;
        ShowRestaurants(restaurants);
    })
});

function ActivarB1(event: Event){
    let button = <HTMLLinkElement> event.currentTarget;
    button.classList.add('active');
}

function ActivarB2(event: Event){
    let button = <HTMLLinkElement> event.currentTarget;
    button.classList.add('active');
}

function DesactivarB1(event: Event){
    let button = <HTMLLinkElement> event.currentTarget;
    button.classList.remove('active');
}

function DesactivarB2(event: Event){
    let button = <HTMLLinkElement> event.currentTarget;
    button.classList.remove('active');
}

function ShowRestaurants(restaurants: Restaurant[]){
    container.innerHTML = '';
    let result = [];
    result = restaurants;
    if(showOpen){
        result = restaurants.filter(e =>{
            if(e.daysOpen.includes(new Date().getDay())){
                return e;
            }
        });
    }if(search !== '' ){
        result = result.filter(e =>{
            if(e.name.toLowerCase().indexOf(search.toLowerCase()) != -1){
                return e;
            }
        });
    }
    if(orderName){
        result.sort((n1, n2):number =>{
            if(n1.name < n2.name) return -1;
            if(n1.name > n2.name) return 1;
            return 0;
        });
    }if(!orderName) {
        result.sort((n1, n2):number =>{
            if(n1.id < n2.id) return -1;
            if(n1.id > n2.id) return 1;
            return 0;
        });
    }
    result.forEach(e =>{
            addRest(e);
    });
}

function addRest(r) {
    let pHTML = <HTMLDivElement> r.toHTML();
    if (r.mine == true){
        pHTML.querySelector("button")
         .addEventListener('click', e => {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                    r.delete().then(resp => pHTML.remove());
                  swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                }
              })
         });
    }
    container.appendChild(pHTML);
}