import {IRestaurant} from '../interfaces/irestaurant';
import {IUser} from '../interfaces/iuser';
import { Http } from './http.class';
import { SERVER, WEEKDAYS } from '../constants';
import { IComment } from '../interfaces/icomment';
declare function require(module: string) : any;
let template = require('../../templates/restaurant.handlebars');

export class Restaurant implements IRestaurant {
    id?: number;
    name: string;
    description: string;
    cuisine: string[];
    daysOpen: number[];
    image: string;
    phone: string;
    creator?: IUser;
    mine?: boolean;
    distance?: number;
    commented?: boolean;
    stars?: number;
    address: string;
    lat: number;
    lng: number;

    constructor(restJSON: IRestaurant) {
        this.id = restJSON.id || -1;
        this.name = restJSON.name;
        this.description = restJSON.description;
        this.cuisine = restJSON.cuisine;
        this.daysOpen = restJSON.daysOpen.map(day => +day);
        this.image = restJSON.image;
        this.phone = restJSON.phone;
        this.creator = restJSON.creator;
        this.mine = restJSON.mine;
        this.distance = restJSON.distance;
        this.commented = restJSON.commented;
        this.stars = restJSON.stars;
        this.address = restJSON.address;
        this.lat = restJSON.lat;
        this.lng = restJSON.lng;
    }

    static async getAll() : Promise<Restaurant[]> {
        let response = await Http.get(`${SERVER}/restaurants`);
        return response.restaurants.map(rest =>new Restaurant(rest));
    }
    
    static async get(id: number) : Promise<Restaurant> {
        let response = await Http.get(`${SERVER}/restaurants/${id}`);
        return new Restaurant(response.restaurant);
    }

    async post() : Promise<Restaurant> {
        let response = await Http.post(`${SERVER}/restaurants`, this);
        return new Restaurant(response.restaurant);
    }

    async delete() : Promise<void> {
        return await Http.delete(`${SERVER}/restaurants/${this.id}`);
    }

    async getComments() : Promise<IComment> {
        return await Http.get(`${SERVER}/restaurants/${this.id}/comments`);
    }

    async addComment(comment: IComment) : Promise<IComment> {
        return await Http.post(`${SERVER}/restaurants/${this.id}/comments`, comment);
    }

    toHTML() : HTMLDivElement {
        let open: boolean;

        if(this.daysOpen.includes(new Date().getDay())) {
            open = true;
        } else {
            open = false;
        }
        let fullStars = (new Array(Math.round(this.stars))).fill(1);
        let emptyStars = (new Array(5 - Math.round(this.stars))).fill(1);

        let rest: any = {
            id: this.id,
            name: this.name,
            description: this.description,
            daysOpen: this.daysOpen.map(d => WEEKDAYS[+d]).join(', '),
            phone: this.phone,
            image: `${SERVER}/${this.image}`,
            address: this.address,
            cuisine: this.cuisine,
            lat: this.lat,
            lng: this.lng,
            mine: this.mine,
            distance: (this.distance).toFixed(2),
            open: open,
            fullStars: fullStars,
            emptyStars: emptyStars,
            stars: this.stars
        }

        let div = <HTMLDivElement> document.createElement('div')
        div.classList.add('card');

        let htmlRest = template(rest);
        div.innerHTML = htmlRest;

        return div;
    }
}