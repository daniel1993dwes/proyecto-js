import {IUser} from '../interfaces/iuser';
import {Http} from './http.class';
import {SERVER} from '../constants';
declare function require(module: string) : any;
let template = require('../../templates/profile.handlebars');

export class User implements IUser {
    id?: number;    name?: string;
    avatar?: string;
    email: string;
    password?: string;
    lat?: number;
    lng?: number;
    me?: boolean;
    constructor(userInfo: IUser) {
        this.id = userInfo.id || -1;
        this.name = userInfo.name;
        this.avatar = userInfo.avatar;
        this.email = userInfo.email;
        this.password = userInfo.password;
        this.lat = userInfo.lat;
        this.lng = userInfo.lng;
        this.me = userInfo.me;
    }

    static async getProfile(id?: number) : Promise<User> {
        if(id != null || id!= undefined) {
            let resp = await Http.get(`${SERVER}/users/${id}`);
            return new User(resp['user']);
                 
        } else {
            let resp = await Http.get(`${SERVER}/users/me`);
                return new User(resp['user']);
        }
        
    }

    static async saveProfile(name: string, email: string) : Promise<void> {
        return await Http.put(`${SERVER}/users/me`, {name, email});
    }

    static async saveAvatar(avatar: string) : Promise<string> {
        return Http.put(`${SERVER}/users/me/avatar`, {avatar})
            .then(resp => {
                return resp.avatar;
            })
    }

    static async savePassword(password: string) : Promise<void> {
        return await Http.put(`${SERVER}/users/me/password`,{password});
    }

    toHTML() : HTMLDivElement {
        let div = <HTMLDivElement> document.createElement('div');
        
        let user: IUser = {
            name: this.name,
            avatar: `${SERVER}/${this.avatar}`,
            email: this.email,
            me: this.me
        }

        let htmlUser = template(user);
        div.innerHTML = htmlUser;

        return div;
    }
}