import { IResponse } from "../interfaces/iresponse";

export class Http {
    /**
     * Makes an AJAX call to the server
     * @param {string} method GET, POST, PUT, DELETE
     * @param {string} url Server's url
     * @param {{}} headers Header like 'Content-Type'
     * @param {string} body Data to send to the server
     * @returns {Promise<any>} Data received in JSON format
     */
    static ajax(method: string, url: string, headers:any = {}, body:any = null) : Promise<IResponse> {
        const token = localStorage.getItem('token');
        if(token) headers.Authorization = 'Bearer ' + token;
        return fetch(url, {method, headers, body})
            .then(resp => {
                if(resp.status >= 300){
                    return Promise.reject(resp.json());
                }
                return resp.json();
            }); 
    }
    static get(url: string): Promise<any> {
        return Http.ajax('GET', url);
    }

    /**
     * @param {string} url 
     * @param {{}} data Data in JSON format 
     */
    static post(url: string, data: {}): Promise<any> {
        return Http.ajax('POST', url, {
            'Content-Type':'application/json'
        }, JSON.stringify(data));
    }

    /**
     * @param {string} url 
     * @param {{}} data Data in JSON format 
     */
    static put(url: string, data: {}): Promise<any> {
        return Http.ajax('PUT', url, {
            'Content-Type':'application/json'
        }, JSON.stringify(data));
    }
    
    static delete(url): Promise<any> {
        return Http.ajax('DELETE', url);
    }
}