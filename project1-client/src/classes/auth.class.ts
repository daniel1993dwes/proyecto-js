import { IUser } from '../interfaces/iuser';
import {SERVER} from '../constants';
import { Http } from './http.class';

export class Auth {
    static async register(userInfo: IUser) : Promise<void> {
        return await Http.post(`${SERVER}/auth/register`,userInfo);
    }

    static async login(userInfo: IUser) : Promise<void> {
        return await Http.post(`${SERVER}/auth/login`, userInfo);
    }

    static async checkToken() : Promise<void> {
        return await Http.get(`${SERVER}/auth/validate`);
    }

    static logout() {
        localStorage.removeItem('token');
        window.location.assign('../../login.html');
    }
}
