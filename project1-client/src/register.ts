'use strict';
import {IUser} from './interfaces/iuser';
import {Geolocation} from './classes/geolocation.class'
import {Auth} from './classes/auth.class';
import swal from 'sweetalert2';
 
document.addEventListener('DOMContentLoaded',event => {
    let formRegister:HTMLFormElement = <HTMLFormElement> document.getElementById('form-register');
    escribirCoordenadas();
    formRegister.avatar.addEventListener('change', event =>{
        convertToBase64(event);
    });
    formRegister.addEventListener('submit', e =>{
        recogerFormulario(formRegister);
    });
        
});

function recogerFormulario(formRegister: HTMLFormElement) {
    event.preventDefault();
    let nombre = (<HTMLInputElement>document.getElementById('name')).value;
    let email = <string> formRegister.email.value;
    let email2 = <string> formRegister.email2.value;
    let password = <string> formRegister.password.value;
    let image = (<HTMLImageElement>document.getElementById('imgPreview')).attributes.getNamedItem('src').value;
    let latitude = <number> formRegister.lat.value;
    let longitude = <number> formRegister.lng.value;
    if (email == email2) {
        let usuarioRegistrado : IUser = {
            name:  nombre,
            email: email,
            password: password,
            avatar: image,
            lat: +latitude,
            lng: +longitude
        }
        enviarAlServidor(usuarioRegistrado);
    } else {
        swal(
            'Registration error',
            'Both emails do not match',
            'error'
        )
        formRegister.email.classList.remove('is-invalid');
        formRegister.email2.classList.remove('is-invalid');
        formRegister.email.classList.add('is-invalid');
        formRegister.email2.classList.add('is-invalid');
    }
}

function escribirCoordenadas() : void {
    let lon = <HTMLInputElement> document.getElementById('lng');
    let lat = <HTMLInputElement> document.getElementById('lat');
    Geolocation.getLocation().then((response) => {
        lon.value = response.longitude.toString();
        lat.value = response.latitude.toString();
    }).catch(error => {
        lon.type = 'hidden';
        lat.type = 'hidden';
    });
}

function convertToBase64(event: any){
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
            (<HTMLImageElement>document.getElementById('imgPreview')).src = <string> reader.result;
    });
}

async function enviarAlServidor(usuarioRegistrado) : Promise<void> {
    try{
        let resp = await Auth.register(usuarioRegistrado);
        swal(
            'User registered!',
            'You can now log in the application',
            'success'
            ).then(() => {
                window.location.replace('../login.html');
        })
    } catch (e) {
        swal(
            'Registration error!',
            'There are empty fields which are required',
            'error'
        );
        let error = <Response> e;
        let errorJson = await error;
        Array.from(errorJson['message']).forEach(ele => {
            document.getElementById(ele['property']).classList.remove('is-invalid');
            document.getElementById(ele['property']).classList.add('is-invalid');
        });
    }           
}