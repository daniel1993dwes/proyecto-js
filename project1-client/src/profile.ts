'use strict';
import { Auth } from "./classes/auth.class";
import { User } from "./classes/user.class";

let queryId = location.search.substring(4);
let container: HTMLDivElement;
let response: User;

document.addEventListener('DOMContentLoaded', async event =>{
    let container = document.getElementById('profile')
    document.getElementById('logout').addEventListener('click', event => {
        Auth.logout();
    })

    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign('../login.html');
    }
    
    try {
        if (queryId === '' || queryId === null) {
            response = await User.getProfile();
        } else {
            response = await User.getProfile(+queryId);
        }
        let uHtml = response.toHTML();
        container.appendChild(uHtml);
    } catch (e) {
        let error = <Response> e;
        let eJSON = await error;
        let h1 = <HTMLHeadingElement> document.createElement('h1');
        h1.textContent = 'Error: ' + eJSON['statusCode'] + ' ' + eJSON['error'];
        container.appendChild(h1);
    }
});