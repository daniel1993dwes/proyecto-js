'use strict';
import { Restaurant } from "./classes/restaurant.class";
import { Auth } from "./classes/auth.class";
import { SERVER } from "./constants";
import { IComment } from "./interfaces/icomment";
import swal from 'sweetalert2';
import { Gmap } from "./classes/gmaps.class";
import { urlSettings } from 'google-maps-promise';

declare function require(module: string): any;
let template = require("../templates/comment.handlebars");

urlSettings.key = 'AIzaSyAX2skeHPuTfAp3kDCq8orUcCFHqhPqXJg';
urlSettings.libraries = ['places'];

let queryId = location.search.substring(4);
let comment: IComment;
let rest: Restaurant;
let commentForm: HTMLFormElement;

document.addEventListener("DOMContentLoaded", async event => {
    document.getElementById('logout').addEventListener('click', event => {
        Auth.logout();
    })
    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign('../login.html');
    }
    obtenerEstrellas();
    commentForm = <HTMLFormElement>document.getElementById('commentForm');
    document.getElementById("logout").addEventListener("click", event => {
        Auth.logout();
    });

    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign("../login.html");
    }

    await verRestaurante();

    commentForm.addEventListener('submit',async event => {
        event.preventDefault();
        comment = CommentWrite();
        try{
            let response = await rest.addComment(comment);
            swal(
                'Commented!',
                'Your comment has been saved!',
                'success'
                ).then(() => {
                    let liHTML = toHTMLComment(response['comment']);
                    document.getElementById("comments").appendChild(liHTML);
                    commentForm.style.display = 'none';
            });
        } catch(e) {
            swal(
                'Comment Error!',
                'An error has ocurred',
                'error'
            );
        }
    })
});

async function verRestaurante() : Promise<void> {
    try {
        rest = await Restaurant.get(parseInt(queryId));
        let pHTML = <HTMLDivElement>rest.toHTML();
        document.getElementById("cardContainer").appendChild(pHTML);
        await cargarGoogle({latitude: rest.lat, longitude: rest.lng});
        let comments = await rest.getComments();
        comments["comments"].forEach(element => {
        let liHTML = toHTMLComment(element);
        document.getElementById("comments").appendChild(liHTML);
        });
        restCommented(rest, commentForm);
    } catch (e) {
        let error = <Response>e;
        let errorJSON = await error;
        document.getElementById("cardContainer").innerHTML =
        "<h1>Error " +
        errorJSON["statusCode"] +
        " " +
        errorJSON["error"] +
        "</h1>";
        document.getElementById("map").innerHTML =
        "<h3>Page can't show map for this element!!</h3>";
    }
}

function restCommented(rest: Restaurant, commentForm: HTMLFormElement) : void {
    if(rest.commented == true){
        commentForm.style.display = 'none';
    }
}

function toHTMLComment(element): HTMLLIElement {
    let li = <HTMLLIElement>document.createElement("li");
    li.classList.add("list-group-item", "d-flex", "flex-row");

    let fullStars = (new Array(Math.round(element.stars))).fill(1);
    let emptyStars = (new Array(5 - Math.round(element.stars))).fill(1);

    let fecha = new Date(element.date);

    let comment: any = {
    userId: element.user.id,
    userName: element.user.name,
    userImage: `${SERVER}/${element.user.avatar}`,
    comment: element.text,
    date: fecha.toLocaleString(),
    fullStars: fullStars,
    emptyStars: emptyStars
    };

    let HTMLComment = template(comment);
    li.innerHTML = HTMLComment;

    return li;
}

function obtenerEstrellas() : number {
    let starsDiv = document.getElementById('stars');
    let stars = <HTMLIFrameElement[]>Array.from(starsDiv.children);
    let score: number;
    stars.forEach(star => {
        star.addEventListener('click', event => {
            score = +star.dataset['score'];
            stars.forEach(s => {
                if(+s.dataset['score'] <= score){
                    s.classList.remove('far');
                    s.classList.add('fas');
                } else {
                    s.classList.remove('fas');
                    s.classList.add('far');
                }
            })
        })
    })
    return score;
}

function CommentWrite() : IComment {
    let commentario = (<HTMLTextAreaElement> document.getElementById('comment')).value;
    let estrellas = obtenerEstrellas();
    if(estrellas === undefined) estrellas = 1;
    let comm: IComment = {
        stars: +estrellas,
        text: commentario
        
    }
    return comm;
}

async function cargarGoogle(coords) {
    let divMap = <HTMLDivElement> document.getElementById('map');
    let gmap = new Gmap(divMap, coords);
    await gmap.loadMap();
    gmap.createMarker(coords, 'red');
}