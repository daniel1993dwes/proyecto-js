import { Restaurant } from "./classes/restaurant.class";
import { urlSettings } from 'google-maps-promise';
import { Gmap } from './classes/gmaps.class';
import { Geolocation } from "./classes/geolocation.class";
import { Auth } from "./classes/auth.class";

urlSettings.key = 'AIzaSyAX2skeHPuTfAp3kDCq8orUcCFHqhPqXJg';
urlSettings.libraries = ['places'];

'use strict';
let newPlaceForm: HTMLFormElement = null;

document.addEventListener("DOMContentLoaded", async event => {
    document.getElementById('logout').addEventListener('click', event => {
        Auth.logout();
    })
    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign('../login.html');
    }
    let coords = await Geolocation.getLocation();
    let divMap = <HTMLDivElement> document.getElementById('map');
    let gmap = new Gmap(divMap, coords);
    await gmap.loadMap();
    gmap.createMarker(coords, 'red');

    const map = gmap.loadMap();

    gmap.createAutocomplete(<HTMLInputElement>document.getElementById('address'));

    newPlaceForm = <HTMLFormElement> document.getElementById("newPlace");
    newPlaceForm.image.addEventListener('change', event =>{
        loadImage(event);
    });
    newPlaceForm.addEventListener('submit', event =>{
        validateForm(newPlaceForm, coords);
    });
});

function testInputExpr(input: HTMLInputElement, expr: RegExp) : boolean {
    input.classList.remove("is-valid", "is-invalid");
    if (expr.test(input.value)) {
        input.classList.add("is-valid");
        return true;
    } else {
        input.classList.add("is-invalid");
        return false;
    }
}

function validatePhone() : boolean {
    let input = <HTMLInputElement> document.getElementById("phone");
    return testInputExpr(input, /[0-9]+(\.[0-9]{1,2})?/);
}

function validateName() : boolean {
    let input = <HTMLInputElement> document.getElementById("name");
    return testInputExpr(input, /[a-z][a-z ]*/);
}

function validateDescription() : boolean{
    let input = <HTMLInputElement> document.getElementById("description");
    return testInputExpr(input, /.*\S.*/);
}

function validateCuisine() : boolean {
    let input = <HTMLInputElement> document.getElementById("cuisine");
    return testInputExpr(input, /.*\S.*/);
}

function validateDays(daysArray: number[]) : boolean {
    let daysError = <HTMLDivElement> document.getElementById('daysError');
    if(!daysArray.length)
        daysError.classList.remove('d-none');
    else
        daysError.classList.add('d-none');
    return daysArray.length > 0;
}

function validateImage() : boolean {
    let imgInput = <HTMLInputElement> document.getElementById("image");

    imgInput.classList.remove("is-valid", "is-invalid");
    if(imgInput.files.length > 0) {
        imgInput.classList.add("is-valid");
        return true;
    } else {
        imgInput.classList.add("is-invalid");
        return false;
    }
}

async function validateForm(newPlaceForm: HTMLFormElement, coords: Coordinates) : Promise<void> {
    event.preventDefault();
    let name = (<HTMLInputElement> document.getElementById('name')).value;
    let photo = (<HTMLImageElement> document.getElementById("imgPreview")).src;
    let cuisine = (<HTMLInputElement> newPlaceForm.cuisine).value;
    let arrcuisine =<string[]> Array.from(cuisine.split(',').map(e => e.trim()));
    let desc = <string> newPlaceForm.description.value;
    let phone = <string> newPlaceForm.phone.value;
    let daysChecks = <HTMLInputElement[]> newPlaceForm.days;
    let days = Array.from(daysChecks).filter(dc => dc.checked).map(dc => +dc.value);
    let address = <string> newPlaceForm.address.value;
    console.log(coords.latitude + ' ' + coords.longitude);

    if (validateName() && validateDescription() && validateCuisine() &&
    validateDays(days) && validatePhone() && validateImage()) {
        const rest = new Restaurant({
            name: name,
            image: photo,
            cuisine: arrcuisine,
            description: desc,
            phone: phone,
            daysOpen: days,
            address: address,
            lat: coords.latitude,
            lng: coords.longitude

        });

        try {
            let resp = await rest.post();
            window.location.assign('../index.html');
        } catch(error) {
            let e = <Response> error;
            let errorJSON = await e;
            Array.from(errorJSON['message']).forEach(ele => {
                document.getElementById(ele['property']).classList.remove('is-invalid');
                document.getElementById(ele['property']).classList.add('is-invalid');
            });
        };
    }
}

function loadImage(event) : void {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        (<HTMLImageElement>document.getElementById("imgPreview")).src = <string> reader.result;
    });
}