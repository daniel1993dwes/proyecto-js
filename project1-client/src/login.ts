'use strict';
import { Geolocation } from "./classes/geolocation.class";
import { Auth } from "./classes/auth.class";
import { IUser } from "./interfaces/iuser";

document.addEventListener('DOMContentLoaded', event => {
    validarToken();
    let formLogin = <HTMLFormElement>document.getElementById('form-login');
    formLogin.addEventListener('submit', event => {
        mandarAlServidor(event, formLogin)
    })
});

async function mandarAlServidor(event: Event, formLogin: HTMLFormElement) : Promise<void> {
    event.preventDefault();
    let coords = await Geolocation.getLocation();

    let userInfo: IUser = {
        email: formLogin.email.value,
        password: formLogin.password.value,
        lat: +coords.latitude,
        lng: +coords.longitude
    }

    try {
        let resp = await Auth.login(userInfo);
        localStorage.setItem('token',resp['accessToken']);
        window.location.assign('../index.html');
    } catch(error) {
        error.then(e => {
            document.getElementById('errorInfo').innerHTML = e.error
        })
    };
}

async function validarToken() : Promise<void> {
    try {
       await Auth.checkToken();
       window.location.assign('../index.html');
    } catch(e) {
        localStorage.removeItem('token');
    }
    
}