'use strict';
import { User } from "./classes/user.class";
import { Auth } from "./classes/auth.class";
import { SERVER } from "./constants";

let containerError = HTMLBodyElement;
let form: HTMLCollectionOf<HTMLFormElement>;
let name: HTMLInputElement;
let email: HTMLInputElement;
let avatar: HTMLImageElement;
let image: HTMLInputElement;
let pass1: HTMLInputElement;
let pass2: HTMLInputElement;
let previo: HTMLImageElement;

document.addEventListener('DOMContentLoaded', async event => {
    form = <HTMLCollectionOf<HTMLFormElement>> document.getElementsByTagName('form');
    email = <HTMLInputElement> document.getElementById('email');
    name = <HTMLInputElement> document.getElementById('name');
    avatar = <HTMLImageElement> document.getElementById('avatar');
    image = <HTMLInputElement> document.getElementById('image');
    pass1 = <HTMLInputElement> document.getElementById('password');
    pass2 = <HTMLInputElement> document.getElementById('password2');
    previo = <HTMLImageElement> document.getElementById('imgPreview');

    image.addEventListener('change', event =>{
        loadImage(event);
    });

    document.getElementById('logout').addEventListener('click', event => {
        Auth.logout();
    })

    try {
        await Auth.checkToken();
    } catch (e) {
        window.location.assign('../login.html');
    }

    try {
        let response = await User.getProfile();
        email.value = response.email;
        name.value = response.name;
        avatar.src = `${SERVER}/${response.avatar}`;
    } catch (e) {
        let error = <Response> e;
        let eJSON = await error;
        let h1 = <HTMLHeadingElement> document.createElement('h1');
        h1.innerHTML = 'Error ' + eJSON;
    }

    Array.from(form).forEach(element =>{
        let button = <HTMLButtonElement> element.querySelector('button');
        element.addEventListener('submit', event =>{
            if(button.textContent == 'Edit profile') cambiarPerfil();
            if(button.textContent == 'Edit avatar') cambiarAvatar();
            if(button.textContent == 'Edit password') cambiarPassword();
        })
    })
});

async function cambiarAvatar() {
    event.preventDefault();
    try {
        await User.saveAvatar(previo.src);
        avatar.src = previo.src;
        previo.classList.add('d-none');
        (<HTMLParagraphElement>document.getElementById('errorInfo2')).innerHTML = '';
            (<HTMLParagraphElement>document.getElementById('okInfo2')).innerHTML = 'Datos cambiados correctamente!!';
    } catch (e) {
        let error = <Response> e;
            let eJSON = await error;
            Array.from(eJSON['message']).forEach(err =>{
                (<HTMLParagraphElement>document.getElementById('okInfo2')).innerHTML = '';
                (<HTMLParagraphElement>document.getElementById('errorInfo2')).innerHTML = 'Error: Changing Images';
            });
    }
}

async function cambiarPassword() {
    event.preventDefault();
    if (pass1.value === pass2.value) {
        try {
            await User.savePassword(pass1.value);
            (<HTMLParagraphElement>document.getElementById('errorInfo3')).innerHTML = '';
            (<HTMLParagraphElement>document.getElementById('okInfo3')).innerHTML = 'Datos cambiados correctamente!!';
        } catch (e) {
            let error = <Response> e;
            let eJSON = await error;
            Array.from(eJSON['message']).forEach(err =>{
                (<HTMLParagraphElement>document.getElementById('okInfo3')).innerHTML = '';
                (<HTMLParagraphElement>document.getElementById('errorInfo3')).innerHTML = 'Error: Changing Passwords';
            });
        }
    } else {
        (<HTMLParagraphElement>document.getElementById('okInfo3')).innerHTML = '';
        (<HTMLParagraphElement>document.getElementById('errorInfo3')).innerHTML = 'Error: both passwords should be equal';
    }
}

async function cambiarPerfil() {
    event.preventDefault();
    try {
        await User.saveProfile(name.value, email.value);
        (<HTMLParagraphElement>document.getElementById('errorInfo1')).innerHTML = '';
        (<HTMLParagraphElement>document.getElementById('okInfo1')).innerHTML = 'Datos cambiados correctamente!!';
    } catch (e) {
        let error = <Response> e;
        let eJSON = await error;
        Array.from(eJSON['message']).forEach(err =>{
            (<HTMLParagraphElement>document.getElementById('okInfo1')).innerHTML = '';
            (<HTMLParagraphElement>document.getElementById('errorInfo1')).innerHTML = 'Error: ' + err['property'] + ' shouldn\'t be empty';
        });
    }
}

function loadImage(event: any) : void {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        previo.src = <string> reader.result;
        previo.classList.remove('d-none');
    });
}